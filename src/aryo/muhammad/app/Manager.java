package aryo.muhammad.app;

import aryo.muhammad.element.*;
import aryo.muhammad.frames.CentralFrame;
import java.awt.*;
import javax.swing.JTextField;

public class Manager
{
  
  CentralFrame theFrame = null;

  byte currentDicePos = 1;
  byte nrOfPlayers = 4;
  byte turnID = 0;
  Player thePlayers[] = new Player[nrOfPlayers];
  Storage theStorages[] = new Storage[nrOfPlayers];
  Home theHomes[] = new Home[nrOfPlayers];
  public Field playingFields[][] = new Field[nrOfPlayers][20];
  Color theColors[] = new Color[nrOfPlayers];
  public JTextField[][] theFields = new JTextField[nrOfPlayers][20];

  public Manager()
  {
    super();
  }

  public byte getCurrentDicePos()
  {
    return currentDicePos;
  }

  public void setCurrentDicePos(byte currentDicePos)
  {
    this.currentDicePos = currentDicePos;
  }

  public Field[][] getPlayingFields()
  {
    return playingFields;
  }

  public void setPlayingFields(Field[][] playingFields)
  {
    this.playingFields = playingFields;
  }

  public JTextField[][] getTheFields()
  {
    return theFields;
  }

  public void setTheFields(JTextField[][] theFields)
  {
    this.theFields = theFields;
  }

  public CentralFrame getTheFrame()
  {
    return theFrame;
  }

  public void setTheFrame(CentralFrame theFrame)
  {
    this.theFrame = theFrame;
    // <editor-fold defaultstate="collapsed" desc="theFields">
    theFields[0][0] = theFrame.getjTextField1();
    theFields[0][1] = theFrame.getjTextField2();
    theFields[0][2] = theFrame.getjTextField3();
    theFields[0][3] = theFrame.getjTextField4();
    theFields[0][4] = theFrame.getjTextField5();
    theFields[0][5] = theFrame.getjTextField6();
    theFields[0][6] = theFrame.getjTextField7();
    theFields[0][7] = theFrame.getjTextField8();
    theFields[0][8] = theFrame.getjTextField9();
    theFields[0][9] = theFrame.getjTextField10();
    theFields[0][10] = theFrame.getjTextField11();
    theFields[0][11] = theFrame.getjTextField12();
    theFields[0][12] = theFrame.getjTextField13();
    theFields[0][13] = theFrame.getjTextField14();
    theFields[0][14] = theFrame.getjTextField15();
    theFields[0][15] = theFrame.getjTextField16();
    theFields[0][16] = theFrame.getjTextField17();
    theFields[0][17] = theFrame.getjTextField18();
    theFields[0][18] = theFrame.getjTextField19();
    theFields[0][19] = theFrame.getjTextField20();

    theFields[1][0] = theFrame.getjTextField21();
    theFields[1][1] = theFrame.getjTextField22();
    theFields[1][2] = theFrame.getjTextField23();
    theFields[1][3] = theFrame.getjTextField24();
    theFields[1][4] = theFrame.getjTextField25();
    theFields[1][5] = theFrame.getjTextField26();
    theFields[1][6] = theFrame.getjTextField27();
    theFields[1][7] = theFrame.getjTextField28();
    theFields[1][8] = theFrame.getjTextField29();
    theFields[1][9] = theFrame.getjTextField30();
    theFields[1][10] = theFrame.getjTextField31();
    theFields[1][11] = theFrame.getjTextField32();
    theFields[1][12] = theFrame.getjTextField33();
    theFields[1][13] = theFrame.getjTextField34();
    theFields[1][14] = theFrame.getjTextField35();
    theFields[1][15] = theFrame.getjTextField36();
    theFields[1][16] = theFrame.getjTextField37();
    theFields[1][17] = theFrame.getjTextField38();
    theFields[1][18] = theFrame.getjTextField39();
    theFields[1][19] = theFrame.getjTextField40();

    theFields[2][0] = theFrame.getjTextField41();
    theFields[2][1] = theFrame.getjTextField42();
    theFields[2][2] = theFrame.getjTextField43();
    theFields[2][3] = theFrame.getjTextField44();
    theFields[2][4] = theFrame.getjTextField45();
    theFields[2][5] = theFrame.getjTextField46();
    theFields[2][6] = theFrame.getjTextField47();
    theFields[2][7] = theFrame.getjTextField48();
    theFields[2][8] = theFrame.getjTextField49();
    theFields[2][9] = theFrame.getjTextField50();
    theFields[2][10] = theFrame.getjTextField51();
    theFields[2][11] = theFrame.getjTextField52();
    theFields[2][12] = theFrame.getjTextField53();
    theFields[2][13] = theFrame.getjTextField54();
    theFields[2][14] = theFrame.getjTextField55();
    theFields[2][15] = theFrame.getjTextField56();
    theFields[2][16] = theFrame.getjTextField57();
    theFields[2][17] = theFrame.getjTextField58();
    theFields[2][18] = theFrame.getjTextField59();
    theFields[2][19] = theFrame.getjTextField60();

    theFields[3][0] = theFrame.getjTextField61();
    theFields[3][1] = theFrame.getjTextField62();
    theFields[3][2] = theFrame.getjTextField63();
    theFields[3][3] = theFrame.getjTextField64();
    theFields[3][4] = theFrame.getjTextField65();
    theFields[3][5] = theFrame.getjTextField66();
    theFields[3][6] = theFrame.getjTextField67();
    theFields[3][7] = theFrame.getjTextField68();
    theFields[3][8] = theFrame.getjTextField69();
    theFields[3][9] = theFrame.getjTextField70();
    theFields[3][10] = theFrame.getjTextField71();
    theFields[3][11] = theFrame.getjTextField72();
    theFields[3][12] = theFrame.getjTextField73();
    theFields[3][13] = theFrame.getjTextField74();
    theFields[3][14] = theFrame.getjTextField75();
    theFields[3][15] = theFrame.getjTextField76();
    theFields[3][16] = theFrame.getjTextField77();
    theFields[3][17] = theFrame.getjTextField78();
    theFields[3][18] = theFrame.getjTextField79();
    theFields[3][19] = theFrame.getjTextField80();
    // </editor-fold>
  }

  public void proceed(JTextField field)
  {
    byte playerPos = 0;
    byte fieldPos = 0;
    Field theField = null;
    byte currentVal = 0;

    playerPos = this.getPlayerPos(field);
    fieldPos = this.getFieldPos(field);
    System.out.println("proceed start");
    theField = playingFields[playerPos][fieldPos];

    if (theField.getContent() > 0)
    {
      System.out.println("one");
      if (theField.getTheLocation().getBackground().equals(theColors[this.getTurnID()]));
      // if (theField.getTheIcons()[0].getTheOwner().equals(this.getTurnID()))
      {
        System.out.println("two");
        if (theField.getClass().equals(Storage.class)) // mengeluarkan ikon dari storage
        {
          System.out.println("three");
          if (getCurrentDicePos() == 6)
          {
            System.out.println("four");
            currentVal = playingFields[playerPos][9].getContent(); // awal // menambah angka di kotak start
            playingFields[playerPos][9].setContent((byte) (currentVal + 1));
            theFields[playerPos][9].setText(String.valueOf(currentVal + 1));
            theFields[playerPos][9].setBackground(theColors[this.getTurnID()]); // akhir
            currentVal = playingFields[playerPos][fieldPos].getContent(); // awal // mengurangkan angka di kotak start
            playingFields[playerPos][fieldPos].setContent((byte) (currentVal - 1));
            if (currentVal > 1)
            {
              theFields[playerPos][fieldPos].setText(String.valueOf(currentVal - 1));
            } else
            {
              theFields[playerPos][fieldPos].setText("");
              theFields[playerPos][fieldPos].setBackground(Color.white);
            }
          } else
          {
            System.out.println("five");
          }
        } else if (theField.getClass().equals(Home.class))
        {
          // nothing to be done at Home // tidak ada yang dilakukan di Home
        } else // in other fields // di fields yang lain
        {
          System.out.println("six");
          for (int i = 1; i <= this.currentDicePos; i++)
          {
            theFields[playerPos][fieldPos + i - 1].setBackground(Color.white);
            theFields[playerPos][fieldPos+i].setBackground(theColors[this.getTurnID()]);
            try
            {
              Thread.sleep(2000);
            } catch(Exception e)
            {
              System.out.println(e);
            }

          }

        }
      }
    }
    System.out.println("1 proceed end");
  }

  public byte getPlayerPos(JTextField field)
  {
    boolean found = false;
    byte playerPos = 0;

    for(int i = 0; i < nrOfPlayers; i++)
    {
      for (int j = 0; j < 20; j++)
      {
        if (field.equals(theFields[i][j]))
        {
          found = true;
          playerPos = (byte) i;
          break;
        }
      }
      if (found == true)
      {
        break;
      }
    }

    return playerPos;
  }

  public byte getFieldPos(JTextField field)
  {
    boolean found = false;
    byte fieldPos = 0;

    for(int i = 0; i < nrOfPlayers; i++)
    {
      for (int j = 0; j < 20; j++)
      {
        if (field.equals(theFields[i][j]))
        {
          found = true;
          fieldPos = (byte) j;
          break;
        }
      }
      if (found == true)
      {
        break;
      }
    }

    return fieldPos;
  }
  
  public void changeColorTurn(Color c)
  {
    theFrame.getjTextField81().setBackground(c);
  }

  public Color[] getTheColors()
  {
    return theColors;
  }

  public void setTheColors(Color[] theColors)
  {
    this.theColors = theColors;
  }

  public Home[] getTheHomes()
  {
    return theHomes;
  }

  public void setTheHomes(Home[] theHomes)
  {
    this.theHomes = theHomes;
  }

  public Player[] getThePlayers()
  {
    return thePlayers;
  }

  public void setThePlayers(Player[] thePlayers)
  {
    this.thePlayers = thePlayers;
  }

  public Storage[] getTheStorages()
  {
    return theStorages;
  }

  public void setTheStorages(Storage[] theStorages)
  {
    this.theStorages = theStorages;
  }

//  public Field[] getTheFields()
//  {
//    return theFields;
//  }
//
//  public void setTheFields(Field[] theFields)
//  {
//    this.theFields = theFields;
//  }

  public byte getNrOfPlayers()
  {
    return nrOfPlayers;
  }

  public void setNrOfPlayers(byte nrOfPlayers)
  {
    this.nrOfPlayers = nrOfPlayers;
  }

  public byte getTurnID()
  {
    return turnID;
  }

  public void setTurnID(byte turnID)
  {
    this.turnID = turnID;
  }
  
}

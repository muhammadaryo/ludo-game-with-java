package aryo.muhammad.element;

public class Icon
{
  byte theID = 0;
  Player theOwner = null;
  Field currentField = null;
  Field nextField = null;
  boolean isAtStorage = true;
  boolean isAtField = false;
  boolean isAtHome = false;

  public Icon()
  {
    super();
  }

  public Field getCurrentField()
  {
    return currentField;
  }

  public void setCurrentField(Field currentField)
  {
    this.currentField = currentField;
  }

  public boolean isIsAtField()
  {
    return isAtField;
  }

  public void setIsAtField(boolean isAtField)
  {
    this.isAtField = isAtField;
  }

  public boolean isIsAtStorage()
  {
    return isAtStorage;
  }

  public void setIsAtStorage(boolean isAtStorage)
  {
    this.isAtStorage = isAtStorage;
  }

  public Field getNextField()
  {
    return nextField;
  }

  public void setNextField(Field nextField)
  {
    this.nextField = nextField;
  }

  public byte getTheID()
  {
    return theID;
  }

  public void setTheID(byte theID)
  {
    this.theID = theID;
  }

  public Player getTheOwner()
  {
    return theOwner;
  }

  public void setTheOwner(Player theOwner)
  {
    this.theOwner = theOwner;
  }

  public boolean isIsAtHome()
  {
    return isAtHome;
  }

  public void setIsAtHome(boolean isAtHome)
  {
    this.isAtHome = isAtHome;
  }
  
}

package aryo.muhammad.element;

import java.awt.Color;

public class Player
{

  Color theColor = null;
  byte theID = 0;
  Icon theIcons[] = new Icon[4]; // tidak digunakan
  Storage theStorage = null; // tidak digunakan
  Home theHome = null; // tidak digunakan
  boolean finished = false;

  public Player()
  {
    super();
    for (int i = 0; i < 4; i++)
    {
      theIcons[i] = new Icon();
      theIcons[i].setIsAtStorage(true);
      theIcons[i].setTheID((byte) i);
      theIcons[i].setTheOwner(this);
    }

  }

  public boolean isFinished()
  {
    return finished;
  }

  public void setFinished(boolean finished)
  {
    this.finished = finished;
  }

  public Color getTheColor()
  {
    return theColor;
  }

  public void setTheColor(Color theColor)
  {
    this.theColor = theColor;
  }

  public Home getTheHome()
  {
    return theHome;
  }

  public void setTheHome(Home theHome)
  {
    this.theHome = theHome;
  }

  public byte getTheID()
  {
    return theID;
  }

  public void setTheID(byte theID)
  {
    this.theID = theID;
  }

  public Icon[] getTheIcons()
  {
    return theIcons;
  }

  public void setTheIcons(Icon[] theIcons)
  {
    this.theIcons = theIcons;
  }

  public Storage getTheStorage()
  {
    return theStorage;
  }

  public void setTheStorage(Storage theStorage)
  {
    this.theStorage = theStorage;
  }
}
